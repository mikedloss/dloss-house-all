Rails.application.routes.draw do
  # devise stuff
  devise_for :users

  # info route
  get "/info", to: "info#index"

  # games route
  resources :games

  # user profiles
  get "/user/:id", to: "users#show", as: "user_profile"

  # default to here
  root to: "home#index"
end
