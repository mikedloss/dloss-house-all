namespace :export_games do
  task to_csv: :environment do
    require 'csv'

    Time.zone = "EST"
    filename = Time.zone.now.strftime("%Y-%m-%d_games.csv")
    file_location = File.join(Rails.root, 'storage', filename)

    CSV.open(file_location, "wb") do |csv|
      csv << Game.column_names
      Game.all.each_with_index do |game, index|
        csv << game.attributes.values
      end
    end

    puts "done! Find this at #{file_location}"
  end
end
