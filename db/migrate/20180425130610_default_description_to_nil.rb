class DefaultDescriptionToNil < ActiveRecord::Migration[5.2]
  def change
    change_column_default :games, :description, nil
  end
end
