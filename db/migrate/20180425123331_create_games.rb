class CreateGames < ActiveRecord::Migration[5.2]
  def change
    create_table :games do |t|
      t.string :title
      t.string :players
      t.string :play_time
      t.float :difficulty
      t.string :type_of_game
      t.string :category
      t.text :description

      t.timestamps
    end
  end
end
