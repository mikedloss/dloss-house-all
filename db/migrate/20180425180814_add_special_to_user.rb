class AddSpecialToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :is_special, :boolean, default: false
  end
end
