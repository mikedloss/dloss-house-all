json.extract! game, :id, :title, :players, :play_time, :difficulty, :type_of_game, :category, :description, :created_at, :updated_at
json.url game_url(game, format: :json)
