class Game < ApplicationRecord
  validates_presence_of :title, :players, :play_time, :difficulty
end
